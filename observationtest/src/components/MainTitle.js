import React from 'react'

function MainTitle() {
  return (
    <div className = "mainTitle">
        <h1>Observation Test</h1>
    </div>
  );
}

export default MainTitle;