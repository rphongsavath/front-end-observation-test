import'./Tile.css'

export default function Tile ({tile, handleChoice}){

    const handleClick = () =>{
        handleChoice(tile)
        
    }

    return(
        <div 
            className = "tile" 
            style ={{backgroundColor:tile.isCorrect ? "green" : "white"}}
            onClick={handleClick}                 
        >
            <p>{tile.id}</p>
        </div>
                   
                     

    );
}

