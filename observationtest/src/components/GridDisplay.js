import React, { useState, useRef } from 'react';
import Tile from './Tile';
import ClickCounter from './ClickCounter';
import './GridDisplay.css';
import EndGameMessage from './EndGameMessage';

function GridDisplay() {

    const evenNumberList = [];
    const oddNumberList = [];
    const startNumber = 25;
    
    const [currentCountTiles, setCurrentCountTiles] = useState(1)
    const [evenClicks, setEvenClicks] = useState(0)
    const [oddClicks, setOddClicks] = useState(0)
    const [oddTiles, setOddTiles] = useState([])
    const [evenTiles, setEvenTiles] = useState([])
    const [allTiles, setAllTiles] = useState([])
    const [finalPercentage, setFinalPercentage] = useState(0)
    const [seconds, setSeconds] = useState(0);


    const timerId = useRef();

    //counter for timer
    const startTimer = () =>{
        timerId.current = setInterval (() =>{
            setSeconds(prev => prev +1);
        },1000)
    }

    //to stop timer when game ends
    const stopTimer = () => {
      clearInterval(timerId.current);
      timerId.current = 0;  
    }

    //reset timer
    const resetTimer = () => {
        stopTimer();
        if (seconds){
            setSeconds(0)
        }
    }

    //Randomize Array
    const shuffleNumbers = (array) =>
    {
        return array.sort(() =>Math.random() - 0.5);
    }

    //Generate Tile Objects
    const generateTiles = () =>
    {
        
        for (let i = 1; i <= startNumber; i++)
        {          
            const tile = 
            {
                id: i,
                isCorrect: false,
            }
            allTiles.push(tile);

            //Sorts and randomly shuffles the even and odd numbers to arrays   
            if (tile.id % 2 === 0)
            {
                evenNumberList.push(tile);
                shuffleNumbers(evenNumberList);
                setEvenTiles([...evenNumberList])
                
            }
            else
            {
                oddNumberList.push(tile);
                shuffleNumbers(oddNumberList);
                setOddTiles([...oddNumberList])
                
            }// end if 


        }//end for 
    }

    //logic for tile clicking
    const handleChoice = (tile) =>{
        console.log(tile);
        if (tile.id % 2 ===0)
        {
            // update even click counter
            setEvenClicks(evenClicks => evenClicks + 1)
            console.log("The even click count is " + evenClicks);

            //update the tile "isCorrect property"
            setEvenTiles(prevTiles =>{
                return prevTiles.map(tile => {
                if(tile.id ===currentCountTiles)
                {
                    return ({...tile, isCorrect: true})
                }
                else
                {
                    return tile
                } //end if

                }) // end map

            }) // end set function
            
        }
        else
        {
            //update odd click counter
            setOddClicks(oddClicks => oddClicks + 1)
            console.log("The odd click count is " + oddClicks)

            //update the tile "isCorrect property"
            setOddTiles(prevTiles =>{
                return prevTiles.map(tile => {
                if(tile.id ===currentCountTiles)
                {            
                    return ({...tile, isCorrect: true})
                }
                else
                {
                    return tile
                } //end if

                }) //end map

            }) //end set function
        }//end if
        
        //compares selected tile to counter
        if ( tile.id === currentCountTiles)
        {
            // update the current count state to next number
            setCurrentCountTiles(currentCountTiles => currentCountTiles + 1)
            console.log("The correct number is " + currentCountTiles);
        }
        else{
            if(tile.id % 2 === 0)
            {
                //suffles even tiles
                setEvenTiles(shuffleNumbers(evenTiles));
                console.log("Wrong! CurrentCount is:" + currentCountTiles + "The selected tile is:"+ tile.id);
            }
            else{
                //suffles odd tiles
                setOddTiles(shuffleNumbers(oddTiles));
                console.log("Wrong! CurrentCount is:" + currentCountTiles + "The selected tile is:"+ tile.id);
            }

        }// end if
        
    }//end function


    const calculatePercentage = () => {
        //percent formula
        const finalPercentage = (((startNumber/(evenClicks + oddClicks))*100).toFixed(2));
        
        if( currentCountTiles > startNumber)
        {
            stopTimer();
            return(
            //populate Win Message
            <div className= 'winMessage'>
                <h2>Congradulations, you completed the game!</h2>
                <p>Correct Percentage: {finalPercentage} %</p>
                <p>Time: {seconds} seconds</p>
            </div>    
            )
        }//end if

    }
  
   
    //Resets Game
    const startGame = () => {
        setCurrentCountTiles(1);
        setEvenClicks(0);
        setOddClicks(0);
        setSeconds(0);
        generateTiles();
        startTimer();

        console.log("new Game");
    }
  
    return (
    <div className = "container">
        <div>
             <button className ="startButton" onClick = {startGame}>Start Game</button>
        </div>

        <div className = 'Timer'>
            {/* <button onClick = {startTimer}>Start Timer</button>
            <button onClick = {stopTimer}>Stop Timer</button>
            <button onClick = {resetTimer}>Reset Timer</button>
            <br></br>
            <br></br> */}
            <p>Seconds: {seconds} </p>
        </div>

    <div className = "mainNumberDisplay">
            <div className='oddNumberDisplay'>
            {oddTiles.map((tile) => (
                <Tile 
                key = {tile.id}
                tile = {tile}
                isCorrect ={tile.isCorrect}
                handleChoice = {handleChoice}
                />
            ))}
            </div>

    
            <div className = 'evenNumberDisplay'>
            {evenTiles.map((tile) => (
                <Tile 
                key = {tile.id}
                tile = {tile}
                isCorrect = {tile.isCorrect}
                handleChoice = {handleChoice}
                />
            ))}
            </div>   
    </div>
    <div className = "mainCounterDisplay">
        <div className = "oddCounterDisplay">
            <p>Number of Odd Clicks</p>
            <ClickCounter
            count = {oddClicks}
            />
        </div>

        <div className= "evenCounterDisplay">
            <p>Number of Even Clicks</p>
            <ClickCounter
            count = {evenClicks}
            />
        </div>
    </div>

    <div className = 'winMessage'>
        
        <div>{calculatePercentage()}</div>
    </div>

    {/* <div>
        <EndGameMessage 
        finalPercentage = {finalPercentage}
        startGame = {startGame}
        />
    </div> */}



    </div>
    );
}

export default GridDisplay;