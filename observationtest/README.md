## Project Name: 
Front End Observation Test v1

## Project Decription: 
This application displays two grids with numbers from 1 - 25. Once the user starts the application, tiles with the associated numbers will display even and odd grids and a timer will begin. Users will then select the correct ascending sequence of numbers until all the tiles are selected. Once the application ends, a message will display the user's results

## Deployment
1. Copy the application from the GitLab repository
2. Navigate to the application folder
3. Right click in the folder and select "Git Bash Here"
4. Navigate up one folder
5. Type 'npm start' in the Git Bash terminal and application should start local port 3000 in a seperate browser window. If port is already in use, type 'npx kill-port 3000' in the Git Bash terminal. Rerun 'npm start'.
6. Open application folder in Visual Studio to review code. 
7. Click the Start Button on Local Server 3000 to begin the application

## Author: Richy Phongsavath